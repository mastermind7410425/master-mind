#Game of master-mind
A project to imitate the game of mastermind using requests and json

Team :
Khushi S
Akshita Sure
Nancy Verma
Alisha Khatoon

Approach :
First we input the file from the given 5letters.txt file.
Then, we made a possibilites function which :
	(A) makes groups of strings of the characters that are common in the guessed word and that of the main word.
	(B) it returns the list of words which include these permutations and removes all other elements from the list which do not contain any of the possible letters judged based on the feedback.
We are using the verifyWord function to repeat this process based on the output of feedbackand guesses till we get the WIN message. 
