import random
from itertools import permutations as nPr
import json
import requests as rq

def load_data(word_list: str) -> list[str]:
        with open(word_list) as f:
                words = [line.strip() for line in f if line[0] != "\n"]
        return words
    
def initial_word(word_list: list[str]) -> str:
    return random.choice(word_list)

def possibilities(feedback : int , guessed : str) -> str:
     options = load_data("/Users/a91986/Desktop/problems/master-mind/5letters.txt")
     groups = list(map("".join,list(nPr(guessed, feedback))))
     return list(filter(lambda word : any(group in word for group in groups) , options))

def playGame() :
    def verifyWord(guessed : str, feedback : str, message : str) :
        while (message != "You win"):
            fb = int(feedback)
            options = possibilities(fb, guessed)
            guessed = random.choice(options)
            guess = {'id': me, 'guess': guessed}
            correct = session.post(guess_url, json=guess)
            result = correct.json()
            print(result)
            feedback = result['feedback']
            message = result['message']
            
    options = load_data("/Users/a91986/Desktop/problems/master-mind/5letters.txt")
    mm_url = 'https://we6.talentsprint.com/wordle/game/'
    register = mm_url + 'register'
    register_dict = {"mode": "mastermind", "name": "Khushi"}
    session = rq.Session()
    r = session.post(register, json=register_dict)
    print(r.json())
    me = r.json()['id']
    
    creat_url = mm_url + 'create'
    creat_dict = {'id': me, 'overwrite': True}
    rc = session.post(creat_url, json=creat_dict)
    
    guess_url = mm_url + 'guess'
    guess_word = initial_word(options)
    guess = {'id': me, 'guess': guess_word}
    correct = session.post(guess_url, json=guess)
    result = correct.json()
    print(result)
    verifyWord(guess_word, result['feedback'], result['message'])
    
print(playGame())
