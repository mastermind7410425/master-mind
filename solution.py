import random
from itertools import permutations as nPr
import json
import requests as rq

def load_data(word_list: str) -> list[str]:
    with open(word_list) as f:
        words = [line.strip() for line in f if line.strip()]
    return words

def initial_word(word_list: list[str]) -> str:
    return random.choice(word_list)

def filter_input( feedback : str , guessed : str , options : list[str]) -> str:
     groups = list(map( "".join,list(nPr(guessed, int(feedback) ))))
     return list(filter(lambda word : any(group in word for group in groups) , options))


options = load_data("words.txt")
mm_url = 'https://we6.talentsprint.com/wordle/game/'
    

register_url = mm_url + 'register'
register_dict = {"mode": "mastermind", "name": "Khushi"}
session = rq.Session()
r = session.post(register_url, json=register_dict)
me = r.json()['id']
    

create_url = mm_url + 'create'
create_dict = {'id': me, 'overwrite': True}
session.post(create_url, json=create_dict)
    
guess_url = mm_url + 'guess'
guessed = []
while True:
        if "win" in message:
            print(f"You win. Correct word was {guess}")
            break
        guessed.append(guess)
        guess = random.choice(options)
        guess_dict = {'id': me, 'guess': guess}
        correct = session.post(guess_url, json=guess_dict)
        result = correct.json()
        print(f" guess : {guess:10} feedback:{result["feedback"]} ")
        
        message = result["message"]
        
        
        options = filter_input(str(result["feedback"]), guess, [ i for i in options if i not in guessed] )
        