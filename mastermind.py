import random
from itertools import permutations as nPr
import json
import requests as rq

      
def load_data(word_list: str) -> list[str]:
    
	with open(word_list) as f:
		words = [line.strip() for line in f if line[0] != "\n"]
	return words

def initial_word( word_list : str) -> str:
    return random.choice(word_list)

def analyze_response( feedback : str , guessed : str , options : list):
    if not feedback == "win":
        options = filter_input(feedback ,guessed ,options)
    else :
        return "You win"
        
def send_input( options : list[str]) -> str:
    guess = random.choice(options) 
    
def filter_input( feedback : str , guessed : str , options : list[str]) -> str:
     groups = list(map( "".join,list(nPr(guessed, int(feedback) ))))
     print(groups)
     return list(filter(lambda word : any(group in word for group in groups) , options))
 
print(filter_input(3 , "mango", load_data( "5letters.txt")))


# def initialise_session( )
options = load_data("words.txt")
mm_url = 'https://we6.talentsprint.com/wordle/game/'
register = mm_url + 'register'
register_dict = {"mode" : "mastermind", "name" : "Khushi" }
register_with = json.dumps(register_dict)
r = rq.post(register, json=register_dict)
print(r.json())
me = r.json()['id']
creat_url = mm_url + 'create'
creat_dict = {'id' : me, 'overwrite' : True}
rc = rq.post(creat_url, json=creat_dict)

session = rq.Session()

r = session.post(register, json = register_dict)

me = r.json()['id']

creat_url = mm_url + 'create'
creat_dict = {'id' : me, 'overwrite' : True}
rc = session.post(creat_url, json=creat_dict)
guess_url = mm_url + 'guess'

guess = {'id' : me, 'guess' : random.choice(options)}
correct = session.post(guess_url, json=guess)
result = dict(correct.json())
print(result)
print(result["feedback"])
r = session.post(register, json = register_dict)


while( "win"  not in  dict(correct.json())['feedback'] ):
 if(input("do you want to continue ?")):
     
    me = r.json()['id']
    creat_url = mm_url + 'create'
    creat_dict = {'id' : me, 'overwrite' : True}
    rc = session.post(creat_url, json=creat_dict)
    guess_url = mm_url + 'guess'
    options = filter_input( str(result['feedback']) , guess , options)
    guess = random.choice(options)
    guess = {'id' : me, 'guess' : guess}
    correct = session.post(guess_url, json=guess)
    print(correct.json())

  
def possibilities(feedback : int , guessed : str) -> str:
     options = load_data("5letters.txt")
     groups = list(map("".join,list(nPr(guessed, feedback))))
     return list(filter(lambda word : any(group in word for group in groups) , options))
     
def verifyWord(guessed : str, feedback : str) :
	while (not feedback != "win"):
		options = possibilities(feedback, guessed)
		guessed = random.choice(options)



